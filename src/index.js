function reducer(action, state = '') {
  if (action.type === 'DIGIT') {
    return state + action.preLoad;
  } else if (action.type === 'CLEAR') {
    return state;
  } else if (action.type === 'OPERATOR') {
    return state + action.preLoad;
  }
  return state;
};

let store;
let firstNumber = [];
let index = 0;
const digit = document.querySelectorAll('.black');
const text = document.querySelector('.text');
const operator = document.querySelectorAll('.pink');
const equal = document.querySelector('.orange');

//обработка вводимых цифр
function appendNumber(num) {
  //для очистки дисплея после нажатия знака
  if (text.value === '+' || text.value === '-' || text.value === '*' || text.value === '/') {
    text.value = '';
  }

  //цифры или очистка
  if (num >= 0 & num <= 9 || num === '.') {
    store = reducer({ type: 'DIGIT', preLoad: num });
    text.value = text.value + store;
  } else if (num === 'C') {
    store = reducer({ type: 'CLEAR' });
    text.value = '';
    firstNumber = [];
  }
  return num
}

//функциональность +-*/
function operatorButton(but) {
  switch (but) {
    case '+':
      //запись в массив первого числа до знака
      firstNumber[index] = text.value;

      //очистка дисплея
      text.value = ''

      //вывод на дисплей знака
      store = reducer({ type: 'OPERATOR', preLoad: but });
      text.value = text.value + store;

      //увеличение индекса и запись знака в массив
      index = index + 1;
      firstNumber[index] = text.value;

      //увеличения индекса для следующего элемента
      index = index + 1
      break;
    case '-':
      firstNumber[index] = text.value;
      text.value = ''
      store = reducer({ type: 'OPERATOR', preLoad: but });
      text.value = text.value + store;
      index = index + 1
      firstNumber[index] = text.value;
      index = index + 1
      break;
    case '*':
      firstNumber[index] = text.value;
      text.value = ''
      store = reducer({ type: 'OPERATOR', preLoad: but });
      text.value = text.value + store;
      index = index + 1
      firstNumber[index] = text.value;
      index = index + 1
      break;
    case '/':
      firstNumber[index] = text.value;
      text.value = ''
      store = reducer({ type: 'OPERATOR', preLoad: but });
      text.value = text.value + store;
      index = index + 1
      firstNumber[index] = text.value;
      index = index + 1
      break;
  }
}

//навешивание события на каждую кнопку 1-9.С
digit.forEach(button => {
  button.addEventListener('click', () => {
    appendNumber(button.value);
  })
})

//навешивание события на +-*/
operator.forEach(button => {
  button.addEventListener('click', () => {
    operatorButton(button.value);
  })
})

//знак равно выводит на дисплей все что было сделано
equal.addEventListener('click', () => {
  //запись последнего введенного числа в массив
  index = index + 1;
  firstNumber[index] = text.value;

  //выводим массив убирая разделитель (запятую)
  text.value = firstNumber.join('');
  return text.value
})


